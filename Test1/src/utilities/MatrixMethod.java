package utilities;

public class MatrixMethod {
	
	/**
	 * Duplicates an array by returning a new array with each value of the original array repeated twice
	 * @param arr The original array to be duplicated
	 * @return The array that has been duplicated
	 */
	public static int[][] duplicate(int[][] arr){
		//Since we can assume the array is rectangular, the length of every row is the same
		int[][] duplicate = new int[arr.length][arr[0].length*2];
		
		for (int i = 0 ; i < arr.length ; i++) {
			/*Separate variable that increments by two every iteration of the inner loop
			to place the next number of the original array inside the duplicate array
			after the duplicate of the previous number*/
			int duplicateCount=0;
			for (int j = 0 ; j < arr[i].length ; j++) {
				duplicate[i][duplicateCount]=arr[i][j];
				duplicate[i][duplicateCount+1]=arr[i][j];
				duplicateCount+=2;
			}
		}
		return duplicate;
	}
	
	public static void main(String[] args) {
		int[][] arr = {{1,2,3},{4,5,6}};
		int[][] arrDuplicate = MatrixMethod.duplicate(arr);
		
		for (int i = 0 ; i < arrDuplicate.length ; i++) {
			for (int j = 0 ; j < arrDuplicate[i].length ; j++) {
				System.out.print(arrDuplicate[i][j]+" ");
			}
			System.out.println();
		}
		
	}
}
