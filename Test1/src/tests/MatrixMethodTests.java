package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	void testDuplicateSquareArray() {
		//Testing a 3x3 matrix
		int[][] arr = {{1,2,3},{4,5,6},{7,8,9}};
		int[][] arrDuplicate = MatrixMethod.duplicate(arr);
		int[][] expected = {{1,1,2,2,3,3},{4,4,5,5,6,6},{7,7,8,8,9,9}};
		assertArrayEquals(expected,arrDuplicate);
	}
	
	@Test
	void testDuplicateRectangularArrayLongWidth() {
		//Testing a 4x5 matrix
		int[][] arr = {{1,2,3,1,0},{4,5,2,1,4},{2,1,4,6,3},{4,2,4,5,1}};
		int[][] arrDuplicate = MatrixMethod.duplicate(arr);
		int[][] expected = {{1,1,2,2,3,3,1,1,0,0},{4,4,5,5,2,2,1,1,4,4},{2,2,1,1,4,4,6,6,3,3},{4,4,2,2,4,4,5,5,1,1}};
		assertArrayEquals(expected,arrDuplicate);
	}
	
	@Test
	void testDuplicateRectangularArrayLongHeight() {
		//Testing a 5x2 matrix
		int[][] arr = {{1,2},{4,5},{2,1},{4,2},{8,0}};
		int[][] arrDuplicate = MatrixMethod.duplicate(arr);
		int[][] expected = {{1,1,2,2},{4,4,5,5},{2,2,1,1},{4,4,2,2},{8,8,0,0}};
		assertArrayEquals(expected,arrDuplicate);
	}
}
