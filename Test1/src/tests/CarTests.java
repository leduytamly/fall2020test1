package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import automobiles.Car;


class CarTests {

	@Test
	void testConstructorValidation() {
		try {
			//Just calling the constructor to see if an exception is thrown
			new Car(-1);
			fail("Did not throw an exception");
		}
		catch(IllegalArgumentException e) {
			//Car threw the right exception and is caught(Test will pass)
		}
		catch(Exception e) {
			fail("Caught the wrong exception");
		}
	}
	
	@Test
	void testMaxPosition() {
		//Testing if MAX_POSITION is the same for all Car objects
		Car c1 = new Car(10);
		Car c2 = new Car(40);
		assertEquals(100,c1.MAX_POSITION);
		assertEquals(100,c2.MAX_POSITION);
		assertEquals(100,Car.MAX_POSITION);
	}
	
	@Test
	void testGetters() {
		Car c1 = new Car(10);
		//Car always starts at the middle of the number line
		assertEquals(50,c1.getLocation());
		assertEquals(10,c1.getSpeed());
	}
	
	@Test
	void testMoveRight() {
		Car c1 = new Car(20);
		c1.moveRight();
		assertEquals(70,c1.getLocation());
	}
	
	@Test
	void testMoveRightOffLine() {
		Car c1 = new Car(60);
		//When moved to the right, it should not be at 110 but at 100
		c1.moveRight();
		assertEquals(100,c1.getLocation());
		
		//Tests if the ">" sign is correct
		Car c2 = new Car(51);
		//When moved to the right, it should not be at 101 but at 100
		c2.moveRight();
		assertEquals(100,c2.getLocation());
		Car c3 = new Car(49);
		//When moved to the right, it should not be at 100 but at 99
		c3.moveRight();
		assertEquals(99,c3.getLocation());
	}
	
	@Test
	void testMoveLeft() {
		Car c1 = new Car (30);
		c1.moveLeft();
		assertEquals(20,c1.getLocation());
	}
	
	@Test
	void testMoveLeftOffLine() {
		Car c1 = new Car(55);
		//When moved to the left, it should not be at -5 but at 0
		c1.moveLeft();
		assertEquals(0,c1.getLocation());
		
		//Tests if the "<" sign is correct
		Car c2 = new Car(51);
		//When moved to the right, it should not be at -1 but at 0
		c2.moveLeft();
		assertEquals(0,c2.getLocation());
		Car c3 = new Car(49);
		//When moved to the right, it should not be at 0 but at 1
		c3.moveLeft();
		assertEquals(1,c3.getLocation());
	}

	@Test
	void testAccelerate() {
		Car c1 = new Car(20);
		c1.accelerate();
		assertEquals(21,c1.getSpeed());
	}
	
	@Test 
	void testAccelerateTwice() {
		Car c1 = new Car(54);
		//When accelerated consecutively, it should add 2 to the speed
		c1.accelerate();
		c1.accelerate();
		assertEquals(56,c1.getSpeed());
	}
	
	@Test
	void testStop() {
		Car c1 = new Car(60);
		c1.stop();
		assertEquals(0,c1.getSpeed());
	}
}
